﻿using Media_Server_Lite_Standalone.Class;
using Ookii.Dialogs.Wpf;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Windows;

namespace Media_Server_Lite_Standalone
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {

        public MainWindow()
        {
            InitializeComponent();

            DataContext = this;

            Title = Config.App.Name;
            appName.Content = Config.App.Name;
        }

        private void installUpdateBtn_Click(object sender, RoutedEventArgs e)
        {
            Windows.Installation.Main installation = new();
            installation.Show();
            Close();
        }

        private void manageBtn_Click(object sender, RoutedEventArgs e)
        {
            var dialog = new VistaFolderBrowserDialog();
            if (dialog.ShowDialog(this).GetValueOrDefault() && MslUtils.IsFolderValid(dialog.SelectedPath))
            {
                Windows.Manager.Main manager = new(dialog.SelectedPath);
                manager.Show();
                Close();
            }
            else
            {
                MessageBox.Show($"This folder does not seem to be a {Config.App.Name} folder. Try another one or install {Config.App.Name} first.", "Invalid folder", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void uninstallBtn_Click(object sender, RoutedEventArgs e)
        {
            var dialog = new VistaFolderBrowserDialog();
            if (dialog.ShowDialog(this).GetValueOrDefault() && MslUtils.IsFolderValid(dialog.SelectedPath))
            {
                if (MessageBox.Show($"Are you sure you want to uninstall {Config.App.Name}?", "Confirmation", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                {
                    MessageBoxResult keepData = MessageBox.Show($"Do you want to keep data (avatars, database, shared folders and files, ...)?", "Question", MessageBoxButton.YesNoCancel, MessageBoxImage.Question);

                    if (keepData == MessageBoxResult.Cancel) return;

                    Process p = new()
                    {
                        StartInfo = new(Path.Join(dialog.SelectedPath, "deps", "Apache", "Apache24", "bin", "httpd.exe"))
                        {
                            WorkingDirectory = dialog.SelectedPath,
                            CreateNoWindow = true,
                            Arguments = "-k stop",
                            UseShellExecute = true
                        }
                    };

                    if (Environment.OSVersion.Version.Major >= 6)
                    {
                        p.StartInfo.Verb = "runas";
                    }

                    p.Start();
                    p.WaitForExit();

                    p = new()
                    {
                        StartInfo = new(Path.Join(dialog.SelectedPath, "deps", "Apache", "Apache24", "bin", "httpd.exe"))
                        {
                            WorkingDirectory = dialog.SelectedPath,
                            CreateNoWindow = true,
                            Arguments = "-k uninstall",
                            UseShellExecute = true
                        }
                    };

                    if (Environment.OSVersion.Version.Major >= 6)
                    {
                        p.StartInfo.Verb = "runas";
                    }

                    p.Start();
                    p.WaitForExit();

                    string appDir = Path.Join(dialog.SelectedPath, "app");
                    string depsDir = Path.Join(dialog.SelectedPath, "deps");

                    List<string> deletable = new() { depsDir, appDir };

                    if (keepData == MessageBoxResult.No)
                    {
                        deletable.Add(Path.Join(dialog.SelectedPath, "data"));
                    }

                    foreach (var path in deletable)
                    {
                        try
                        {
                            Directory.Delete(path, true);
                        }
                        catch (Exception)
                        {
                            MessageBox.Show("An application is locking the application directory, please close the application that is holding the directory hostage.", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                            return;
                        }
                    }

                    MessageBox.Show($"{Config.App.Name} has been uninstalled correctly", "Success", MessageBoxButton.OK, MessageBoxImage.Information);
                }
            }
            else
            {
                MessageBox.Show($"This folder does not seem to be a {Config.App.Name} folder. Try another one or install {Config.App.Name} first.", "Invalid folder", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
    }
}
