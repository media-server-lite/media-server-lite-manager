﻿using System.Threading.Tasks;

namespace Media_Server_Lite_Standalone.Interface
{
    interface IExecutable<T>
    {
        Task<T> execute();
    }
}
