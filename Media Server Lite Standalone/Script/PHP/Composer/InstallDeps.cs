﻿using Media_Server_Lite_Standalone.Class;
using Media_Server_Lite_Standalone.Interface;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Threading.Tasks;

namespace Media_Server_Lite_Standalone.Script.PHP.Composer
{
    class InstallDeps : Scriptable, IExecutable<object>
    {

        public string php { get; private set; }
        public string mediaPath { get; private set; }
        public string avatarPath { get; private set; }
        public string dbPath { get; private set; }
        public string envPath { get; private set; }

        public InstallDeps(params string[] path) : base(path) {
            php = path[1];
            mediaPath = Path.Join(FolderPath, "..", "data", "media");
            avatarPath = Path.Join(FolderPath, "..", "data", "avatars");
            dbPath = Path.Join(Directory.GetParent(FolderPath).FullName, "data", "database", "database.sqlite");
            envPath = Path.Join(FolderPath, ".env");
        }

        public async Task<object> execute()
        {
            if (!Directory.Exists(mediaPath)) Directory.CreateDirectory(mediaPath);
            if (!Directory.Exists(avatarPath)) Directory.CreateDirectory(avatarPath);

            JunctionPoint.Create(Path.Join(FolderPath, "public", "media"), mediaPath, false);
            JunctionPoint.Create(Path.Join(FolderPath, "public", "images", "avatars"), avatarPath, false);

            File.Copy(Path.Join(FolderPath, ".env.example"), envPath);

            if (!File.Exists(dbPath))
            {
                Directory.CreateDirectory(Directory.GetParent(dbPath).FullName);
                File.Create(dbPath);
            }

            var changeEnvT = changeEnv();

            await doCommand($"{Path.Join(php, "composer.phar")} install -d {FolderPath} --no-dev --optimize-autoloader");

            var distUpgrade = doCommand("artisan distupgrade");
            var keyGenerate = doCommand("artisan key:generate");
            var jwtSecret = doCommand("artisan jwt:secret");
            var viewCache = doCommand("artisan view:cache");

            await changeEnvT;
            await distUpgrade;
            await keyGenerate;
            await jwtSecret;
            await viewCache;

            return null;
        }

        public async Task<object> doCommand(string arguments)
        {
            ProcessStartInfo cmdsi = new(Path.Join(php, "php"))
            {
                Arguments = arguments,
                WorkingDirectory = FolderPath,
                CreateNoWindow = true
            };
            Process p = Process.Start(cmdsi);
            await p.WaitForExitAsync().ConfigureAwait(false); ;
            p.Close();

            return null;
        }

        public async Task<object> changeEnv()
        {
            var envStream = new StreamReader(envPath);

            string line;
            List<string> newLines = new();

            while ((line = envStream.ReadLine()) != null)
            {
                if (line.StartsWith("DB_DATABASE"))
                {
                    newLines.Add($"DB_DATABASE={PathUtils.WinToUnix(dbPath)}");
                }
                else
                {
                    newLines.Add(line);
                }                
            }

            envStream.Close();

            await File.WriteAllLinesAsync(envPath, newLines);

            return null;
        }
    }
}
