﻿using Media_Server_Lite_Standalone.Class;
using Media_Server_Lite_Standalone.Interface;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Threading.Tasks;

namespace Media_Server_Lite_Standalone.Script.PHP
{
    public class P7_4_16 : Scriptable, IExecutable<object>
    {
        public P7_4_16(params string[] path) : base(path) {  }

        public async Task<object> execute()
        {File.Delete(Path.Join(FolderPath, "php.ini-development"));

            var phpIni = new StreamReader(Path.Join(FolderPath, "php.ini-production"));

            string line;
            List<string> newLines = new();

            while ((line = phpIni.ReadLine()) != null)
            {
                line = uncommentLine(line, "extension_dir = \"ext\"");
                line = uncommentLine(line, "extension=openssl");
                line = uncommentLine(line, "extension=pdo_sqlite");
                line = uncommentLine(line, "extension=fileinfo");
                line = uncommentLine(line, "extension=mbstring");
                line = line.Replace(";upload_tmp_dir =", $"upload_tmp_dir = \"{PathUtils.WinToUnix(Path.GetTempPath())}\"");
                newLines.Add(line);
            }

            phpIni.Close();

            await File.WriteAllLinesAsync(Path.Join(FolderPath, "php.ini"), newLines);

            string[] arguments = { 
                "-r \"copy('https://getcomposer.org/installer', 'composer-setup.php'); \"",
                "-r \"if (hash_file('sha384', 'composer-setup.php') === '756890a4488ce9024fc62c56153228907f1545c228516cbf63f885e036d37e9a59d27d63f46af1d4d07ee0f76181c7d3') { echo 'Installer verified'; } else { echo 'Installer corrupt'; unlink('composer-setup.php'); } echo PHP_EOL; \"",
                "composer-setup.php",
                "-r \"unlink('composer-setup.php'); \""
            };
            foreach (string argument in arguments)
            {
                runPhp(argument);
            }

            return null;
        }

        private string uncommentLine(string s, string startsWith)
        {
            if (s.StartsWith($";{startsWith}"))
            {
                s = s.Replace($";{startsWith}", $"{startsWith}");
            }

            return s;
        }

        private void runPhp(string arguments)
        {
            ProcessStartInfo cmdsi = new("php")
            {
                Arguments = arguments,
                WorkingDirectory = FolderPath,
                CreateNoWindow = true
            };
            Process cmd = Process.Start(cmdsi);
            cmd.WaitForExit();
            cmd.Close();
        }
    }
}
