﻿using Media_Server_Lite_Standalone.Class;
using Media_Server_Lite_Standalone.Interface;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Threading.Tasks;

namespace Media_Server_Lite_Standalone.Script.Apache
{
    class A2_4_46 : Scriptable, IExecutable<object>
    {
        public string InstallDir { get; private set; }

        public A2_4_46(params string[] path) : base(path)
        {
            InstallDir = path[1];
        }

        public async Task<object> execute()
        {
            var httpdConf = new StreamReader(Path.Join(FolderPath, "Apache24", "conf", "httpd.conf"));

            string line;
            List<string> newLines = new();

            while ((line = httpdConf.ReadLine()) != null)
            {
                line = line.Replace("Define SRVROOT \"c:/Apache24\"", $"Define SRVROOT \"{PathUtils.WinToUnix(Path.Join(FolderPath, "Apache24"))}\"");
                line = line.Replace("#ServerName www.example.com:80", "ServerName localhost:616");
                line = line.Replace("Listen 80", "Listen 616");
                line = line.Replace("DirectoryIndex index.html", "");
                line = line.Replace("Options Indexes FollowSymLinks", "");
                line = line.Replace("#LoadModule rewrite_module modules/mod_rewrite.so", "LoadModule rewrite_module modules/mod_rewrite.so");
                line = line.Replace("${SRVROOT}/htdocs", PathUtils.WinToUnix(
                    Path.Join(
                        InstallDir,
                        "app"
                        )
                    ));
                line = line.Replace("AllowOverride None", "AllowOverride all");

                newLines.Add(line);
            }

            newLines.Add("Include conf/apache_php-fcgid.conf");

            httpdConf.Close();

            await File.WriteAllLinesAsync(Path.Join(FolderPath, "Apache24", "conf", "httpd.conf"), newLines);

            string apachePhpFcgidConf = $@"LoadModule fcgid_module modules/mod_fcgid.so

FcgidInitialEnv PHPRC ""{InstallDir}\deps\PHP"" 
FcgidMaxRequestLen 10000000

<Files ~ ""\.php$"">
    AddHandler fcgid-script .php
    FcgidWrapper ""{PathUtils.WinToUnix(InstallDir)}/deps/PHP/php-cgi.exe"" .php
    Options ExecCGI FollowSymLinks
</Files>

DirectoryIndex index.php index.html";
            await File.WriteAllTextAsync(Path.Join(FolderPath, "Apache24", "conf", "apache_php-fcgid.conf"), apachePhpFcgidConf);

            Process p = new()
            {
                StartInfo = new(Path.Join(FolderPath, "Apache24", "bin", "httpd.exe"))
                {
                    WorkingDirectory = FolderPath,
                    CreateNoWindow = true,
                    Arguments = "-k install",
                    UseShellExecute = true
                }
            };

            if (System.Environment.OSVersion.Version.Major >= 6)
            {
                p.StartInfo.Verb = "runas";
            }

            p.Start();

            return null;
        }
    }
}
