﻿namespace Media_Server_Lite_Standalone.Script
{

    public abstract class Scriptable
    {
        public string FolderPath { get; private set; }

        public Scriptable(params string[] path)
        {
            FolderPath = path[0];
        }
    }
}
