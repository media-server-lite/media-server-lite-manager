﻿using Media_Server_Lite_Standalone.Interface;
using System.IO;
using System.Threading.Tasks;

namespace Media_Server_Lite_Standalone.Script.AFCGI
{
    class A2_3_10 : Scriptable, IExecutable<object>
    {
        public string InstallDir { get; private set; }

        public A2_3_10(params string[] path) : base(path)
        {
            InstallDir = path[1];
        }
        public async Task<object> execute()
        {
            await Task.Run(() =>
            {
                Directory.CreateDirectory(Path.Join(InstallDir, "deps", "Apache", "Apache24", "modules"));

                File.Move(
                    Path.Join(FolderPath, "mod_fcgid-2.3.10", "mod_fcgid.so"),
                    Path.Join(InstallDir, "deps", "Apache", "Apache24", "modules", "mod_fcgid.so")
                    );
            });

            return null;
        }
    }
}
