﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Media_Server_Lite_Standalone.Dto
{
    public class Dependency
    {
        public string Name { get; set; }

        public string Version { get; set; }

        public string URL { get; set; }

        public string Path { get; set; }

        public string Script { get; set; }
    }
}
