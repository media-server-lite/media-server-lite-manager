﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace Media_Server_Lite_Standalone.Dto
{
    public class GLRelease
    {

        public class Assets
        {
            public class Source
            {
                [JsonProperty("format")]
                public string format { get; set; }

                [JsonProperty("url")]
                public string url { get; set; }
            }

            [JsonProperty("sources")]
            public List<Source> sources;
        }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("tag_name")]
        public string TagName { get; set; }

        [JsonProperty("released_at")]
        public DateTime ReleasedAt { get; set; }

        [JsonProperty("assets")]
        public Assets assets;

        public override string ToString()
        {
            return $"{Name} (Released {ReleasedAt.ToShortDateString()})";
        }
    }
}
