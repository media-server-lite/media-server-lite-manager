﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Media_Server_Lite_Standalone.Config
{
    class GitLab
    {
        public static readonly string URL = "https://gitlab.com/api/v4/projects/";

        public static readonly string ProjectId = "24927196";
    }
}
