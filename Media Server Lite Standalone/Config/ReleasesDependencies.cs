﻿using Media_Server_Lite_Standalone.Dto;
using System.Collections.Generic;
using System.IO;

namespace Media_Server_Lite_Standalone.Config
{
    public class ReleasesDependencies
    {
        private static Dependency php7_4_16 = new Dependency
        {
            Name = "PHP",
            Version = "7.4.16",
            URL = "https://windows.php.net/downloads/releases/php-7.4.16-Win32-vc15-x64.zip",
            Path = "php",
            Script = "PHP.P"
        };

        private static Dependency apache2_4_46 = new Dependency
        {
            Name = "Apache",
            Version = "2.4.46",
            URL = "https://www.apachelounge.com/download/VS16/binaries/httpd-2.4.46-win64-VS16.zip",
            Path = "apache",
            Script = "Apache.A"
        };

        private static Dependency apache_mod_fcgid = new Dependency
        {
            Name = "Apache FastCGI",
            Version = "2.3.10",
            URL = "https://www.apachelounge.com/download/VS16/modules/mod_fcgid-2.3.10-win64-VS16.zip",
            Path = Path.Join("apache", "mod_fcgid"),
            Script = "AFCGI.A"
        };

        public static Dictionary<string, List<Dependency>> rds = new Dictionary<string, List<Dependency>>()
        {
            {
                "v1.5",
                new List<Dependency>() {
                    php7_4_16,
                    apache2_4_46,
                    apache_mod_fcgid
                }
            }
        };
    }
}
