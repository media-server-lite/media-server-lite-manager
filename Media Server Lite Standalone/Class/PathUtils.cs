﻿namespace Media_Server_Lite_Standalone.Class
{
    static class PathUtils
    {
        public static string WinToUnix(string path)
        {
            return path.Replace("\\", "/");
        }
    }
}
