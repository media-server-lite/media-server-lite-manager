﻿using System.IO;
using System.Net;
using System.Threading.Tasks;

namespace Media_Server_Lite_Standalone.Class
{
    public static class MslUtils
    {
        public static bool IsFolderValid(string path)
        {
            string[] neededPaths = {
                "app",
                "data",
                "deps",
                Path.Join("app", "app"),
                Path.Join("app", "database"),
                Path.Join("app", "vendor"),
                Path.Join("app", "public"),
                Path.Join("deps", "Apache"),
                Path.Join("deps", "PHP")
            };

            foreach (var p in neededPaths)
            {
                if (!Directory.Exists(Path.Join(path, p)))
                    return false;
            }

            return true;
        }

        public static async Task<bool> IsServerUp(string url, int timeout = 5000, string method = "HEAD")
        {
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
            request.Timeout = timeout;
            request.Method = method;
            try
            {
                HttpWebResponse response = (HttpWebResponse)await request.GetResponseAsync();
                
                return response.StatusCode == HttpStatusCode.OK;
            }
            catch (WebException)
            {
                return false;
            }
        }
    }
}
