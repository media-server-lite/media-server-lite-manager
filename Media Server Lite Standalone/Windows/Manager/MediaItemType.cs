﻿namespace Media_Server_Lite_Standalone.Windows.Manager
{
    public class MediaItemType
    {
        public string Type { get; set; }
        public string Name { get; set; }
        public string Path { get; set; }
    }
}
