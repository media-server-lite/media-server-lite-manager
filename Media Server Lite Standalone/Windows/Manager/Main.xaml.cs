﻿using Media_Server_Lite_Standalone.Class;
using Ookii.Dialogs.Wpf;
using System.Diagnostics;
using System.IO;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace Media_Server_Lite_Standalone.Windows.Manager
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class Main : Window
    {

        public string MslPath { get; private set; }
        public string DataPath { get; private set; }
        public string MediaPath { get; private set; }

        public string FullURL { get => "http://localhost:616"; }

        private Manager _manager;

        public Main(string mslPath)
        {
            MslPath = mslPath;
            DataPath = Path.Join(mslPath, "data");
            MediaPath = Path.Join(DataPath, "media");
            _manager = Manager.GetManager(MslPath);

            InitializeComponent();

            DataContext = this;

            Title = Config.App.Name;
            appName.Content = Config.App.Name;
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            startStopBtn.IsEnabled = false;
            startStopBtn.Content = "Checking status...";

            MslUtils.IsServerUp(FullURL).ContinueWith(r =>
            {
                Dispatcher.Invoke(() =>
                {
                    startStopBtn.IsEnabled = true;
                    
                    if (r.Result)
                    {
                        startStopBtn.Content = "🤚 Stop";
                        _manager.StartWebServer(true);
                    }
                    else
                    {
                        startStopBtn.Content = "▶ Start";
                    }
                });
            });
        }

        private void startStopBtn_Click(object sender, RoutedEventArgs e)
        {
            startStopBtn.IsEnabled = false;

            if (_manager.IsHttpdRunning)
            {
                startStopBtn.Content = "Stopping...";
                _manager.StopWebServer().ContinueWith(_ =>
                {
                    Dispatcher.Invoke(() =>
                    {
                        startStopBtn.Content = "▶ Start";
                        startStopBtn.IsEnabled = true;
                    });
                });
            }
            else
            {
                startStopBtn.Content = "Starting...";
                _manager.StartWebServer().ContinueWith(_ =>
                {
                    Dispatcher.Invoke(() =>
                    {
                        startStopBtn.Content = "🤚 Stop";
                        startStopBtn.IsEnabled = true;
                    });
                });
            }
        }

        private void OpenMainDirBtn_Click(object sender, RoutedEventArgs e)
        {
            Process.Start("explorer", MslPath);
        }

        private void OpenDataDirBtn_Click(object sender, RoutedEventArgs e)
        {
            Process.Start("explorer", DataPath);
        }

        private void OpenMSLAppBtn_Click(object sender, RoutedEventArgs e)
        {
            var sInfo = new ProcessStartInfo(FullURL)
            {
                UseShellExecute = true,
            };
            Process.Start(sInfo);
        }

        private void openMediaDirBtn_Click(object sender, RoutedEventArgs e)
        {
            Process.Start("explorer", MediaPath);
        }

        private void removeMediaBtn_Click(object sender, RoutedEventArgs e)
        {
            MediaItemType mit = (MediaItemType)MediaItemsListView.SelectedItem;
            FileAttributes attr = File.GetAttributes(mit.Path);

            if (attr.HasFlag(FileAttributes.Directory))
            {
                Directory.Delete(mit.Path);
            }
            else
            {
                File.Delete(mit.Path);
            }

            _ = LoadSharedMedia();
        }

        private void addMediaBtn_Click(object sender, RoutedEventArgs e)
        {
            var dialog = new VistaFolderBrowserDialog();
            if (dialog.ShowDialog(this).GetValueOrDefault())
            {
                string source = dialog.SelectedPath;
                string destination = Path.Join(MediaPath, Path.GetFileName(source));
                FileAttributes attr = File.GetAttributes(source);

                if (!attr.HasFlag(FileAttributes.Directory))
                {
                    MessageBox.Show("Not a valid directory");
                    return;
                }

                Process p = new()
                {
                    StartInfo = new("cmd.exe")
                    {
                        Arguments = $"/k mklink /J \"{destination}\" \"{source}\" && exit"
                    }
                };
                p.Start();
                p.WaitForExit();
                _ = LoadSharedMedia();
            }
        }

        private void TabControl_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (e.Source is TabControl)
            {
                if (tabControl.SelectedItem == MediaTabItem)
                {
                    _ = LoadSharedMedia();
                }
            }
        }

        private async Task<object> LoadSharedMedia()
        {
            MediaItemsListView.Items.Clear();

            loadingMediaLbl.Visibility = Visibility.Visible;
            addMediaBtn.IsEnabled = false;
            removeMediaBtn.IsEnabled = false;

            await Task.Run(() =>
            {
                string[] entries = Directory.GetFileSystemEntries(MediaPath, "*", SearchOption.TopDirectoryOnly);

                foreach (string entry in entries)
                {
                    Dispatcher.Invoke(() =>
                    {
                        FileAttributes file = File.GetAttributes(entry);
                        MediaItemsListView.Items.Add(new MediaItemType() { 
                            Type = file.HasFlag(FileAttributes.Directory) ? "Folder" : "File", 
                            Name = file.HasFlag(FileAttributes.Directory) ? Path.GetFileName(entry) : Path.GetFileName(entry),
                            Path = entry 
                        });
                    });
                }

                Dispatcher.Invoke(() =>
                {
                    loadingMediaLbl.Visibility = Visibility.Collapsed;
                    addMediaBtn.IsEnabled = true;
                });
            });

            return null;
        }

        private void MediaItemsListView_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            removeMediaBtn.IsEnabled = true;
        }

        private void addFileMediaBtn_Click(object sender, RoutedEventArgs e)
        {
            var dialog = new VistaOpenFileDialog();
            if (dialog.ShowDialog(this).GetValueOrDefault())
            {
                string source = dialog.FileName;
                string destination = Path.Join(MediaPath, Path.GetFileName(source));
                FileAttributes attr = File.GetAttributes(source);

                if (attr.HasFlag(FileAttributes.Directory))
                {
                    MessageBox.Show("Not a valid file");
                    return;
                }

                Process p = new()
                {
                    StartInfo = new("cmd.exe")
                    {
                        Arguments = $"/k mklink /H \"{destination}\" \"{source}\" && exit"
                    }
                };
                p.Start();
                p.WaitForExit();
                _ = LoadSharedMedia();
            }
        }
    }
}
