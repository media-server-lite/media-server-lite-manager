﻿using System;
using System.Diagnostics;
using System.IO;
using System.Threading.Tasks;

namespace Media_Server_Lite_Standalone.Windows.Manager
{
    public class Manager
    {
        private static Manager manager;

        private string _root;
        public string Root
        {
            get => _root;
            set
            {
                if (_root != null) throw new CustomException.RootAlreadySetException();

                _root = value;
                HttpdFolder = Path.Join(value, "deps", "Apache", "Apache24");
                HttpdExe = Path.Join(HttpdFolder, "bin", "httpd.exe");
            }
        }

        public string HttpdFolder { get; private set; }

        public string HttpdExe { get; private set; }

        public Process HttpdProcess { get; private set; }

        public bool IsHttpdRunning { get => HttpdProcess != null; }

        private Manager(string root)
        {
            Root = root ?? throw new ArgumentNullException("Root cannot be null on Manager instantiation");
        }

        public static Manager GetManager(string root = null)
        {
            if (manager == null) manager = new Manager(root);
            return manager;
        }

        public async Task<bool> StartWebServer(bool fake = false)
        {
            HttpdProcess = new()
            {
                StartInfo = new(HttpdExe)
                {
                    Arguments = "-k start",
                    WorkingDirectory = Root,
                    CreateNoWindow = true,
                    UseShellExecute = true
                }
            };

            if (System.Environment.OSVersion.Version.Major >= 6)
            {
                HttpdProcess.StartInfo.Verb = "runas";
            }

            await Task.Run(() =>
            {
                if (!fake)
                {
                    HttpdProcess.Start();
                    HttpdProcess.WaitForExit();
                }
            });

            return true;
        }


        public async Task<bool> StopWebServer()
        {
            HttpdProcess = new()
            {
                StartInfo = new(HttpdExe)
                {
                    Arguments = "-k stop",
                    WorkingDirectory = Root,
                    CreateNoWindow = true,
                    UseShellExecute = true
                }
            };


            if (System.Environment.OSVersion.Version.Major >= 6)
            {
                HttpdProcess.StartInfo.Verb = "runas";
            }

            await Task.Run(() =>
            {
                HttpdProcess.Start();
                HttpdProcess.WaitForExit();
            });

            HttpdProcess = null;

            return true;
        }
    }
}
