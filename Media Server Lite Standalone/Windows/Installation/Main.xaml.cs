﻿using Media_Server_Lite_Standalone.Dto;
using Media_Server_Lite_Standalone.Interface;
using Media_Server_Lite_Standalone.Script.PHP.Composer;
using Newtonsoft.Json;
using Ookii.Dialogs.Wpf;
using Serilog;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Markup;
using System.Xml;

namespace Media_Server_Lite_Standalone.Windows.Installation
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class Main : Window
    {

        public ObservableCollection<GLRelease> Releases { get; } = new ObservableCollection<GLRelease>();
        private GLRelease selectedRelease = null;
        private string installDir = null;
        private List<Dependency> dependencies = null;
        private int depCompleted = 0;


        public Main()
        {
            InitializeComponent();

            DataContext = this;

            Title = Config.App.Name;
            appName.Content = Config.App.Name;
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            loadReleases().ContinueWith(r =>
            {
                Dispatcher.Invoke(() =>
                {
                    tabControl.SelectedItem = ReleasesTab;
                    r.Result.ForEach(r => Releases.Add(r));
                });
            });
        }

        private static async Task<List<GLRelease>> loadReleases()
        {
            try
            {
                using (HttpClient client = new HttpClient())
                {
                    HttpResponseMessage response = await client.GetAsync($"{Config.GitLab.URL}/{Config.GitLab.ProjectId}/releases");
                    if (response != null)
                    {
                        string jsonString = await response.Content.ReadAsStringAsync();
                        return JsonConvert.DeserializeObject<List<GLRelease>>(jsonString);
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message);
                Log.Error(ex.StackTrace);
            }
            return null;
        }

        private void releasesListView_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            releaseNextBtn.IsEnabled = true;
        }

        private void releaseNextBtn_Click(object sender, RoutedEventArgs e)
        {
            selectedRelease = Releases[releasesListView.SelectedIndex];

            tabControl.SelectedItem = InstallLocationTab;
        }

        private void buildDependenciesGridRowDefinitions(int howManyAutos = 1)
        {
            DependencyGridsList.RowDefinitions.Clear();

            for (ushort i = 0; i < howManyAutos; i++)
            {
                DependencyGridsList.RowDefinitions.Add(new RowDefinition
                {
                    Height = new GridLength(0, GridUnitType.Auto)
                });
            }

            DependencyGridsList.RowDefinitions.Add(new RowDefinition
            {
                Height = new GridLength(0, GridUnitType.Star)
            });
        }

        private void downloadDepBtn_Click(object sender, RoutedEventArgs e)
        {
            backDepBtn.IsEnabled = false;
            downloadDepBtn.IsEnabled = false;

            string appDir = Path.Join(installDir, "app");
            string depsDir = Path.Join(installDir, "deps");

            List<string> reCreatePaths = new() { appDir, depsDir };

            foreach (var path in reCreatePaths)
            {
                try
                {
                    if (Directory.Exists(path))
                    {
                        Directory.Delete(path, true);
                    }
                    Directory.CreateDirectory(path);
                }
                catch (Exception)
                {
                    MessageBox.Show("An application is locking the installation directory you chose, please go back and chose another installation directory or close the application that is holding the directory hostage.", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                    backDepBtn_Click(null, null);
                    return;
                }
            }

            var children = DependencyGridsList.Children;

            for (ushort i = 0; i < dependencies.Count; i++)
            {
                var statusLbl = (Label)((Grid)children[i + 1]).Children[2];
                var progressBar = (ProgressBar)((Grid)children[i + 1]).Children[1];
                statusLbl.Content = "Downloading...";

                string downloadFileName = Path.Join(depsDir, Path.GetFileName(dependencies[i].URL));
                string name = Path.Join(depsDir, dependencies[i].Name);
                string version = dependencies[i].Version;
                string script = dependencies[i].Script;
                string url = dependencies[i].URL;

                downloadFromURL(url, downloadFileName, (sender, e) => WebClient_DownloadProgressChanged(sender, e, url)).ContinueWith(r =>
                {
                    Dispatcher.Invoke(() =>
                    {
                        statusLbl.Content = "Download completed. Extracting...";
                        progressBar.IsIndeterminate = true;
                    });

                    extractZIP(downloadFileName, name).ContinueWith(z =>
                    {
                        Dispatcher.Invoke(() =>
                        {
                            statusLbl.Content = "Extracted. Configuring...";
                        });

                        File.Delete(downloadFileName);

                        string folderName = Path.GetFileNameWithoutExtension(name).Replace(" ", "_");

                        try
                        {
                            IExecutable<object> scriptClass = (IExecutable<object>)Activator.CreateInstance(
                            Type.GetType(@$"{typeof(MainWindow).Namespace}.Script.{script}{version.Replace(".", "_")}"), name, installDir
                            );

                            scriptClass.execute().ContinueWith(s =>
                            {
                                depCompleted++;

                                Dispatcher.Invoke(() =>
                                {
                                    statusLbl.Content = "Finished";
                                    progressBar.IsIndeterminate = false;

                                    if (depCompleted == dependencies.Count)
                                    {
                                        backDepBtn.IsEnabled = true;
                                        nextDepBtn.IsEnabled = true;
                                    }
                                });
                            });
                        }
                        catch (Exception)
                        {
                            MessageBox.Show($"Fatal error. No script configurator for {name}. Application will close.", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                            Dispatcher.Invoke(() => Close());
                        }
                    });
                });
            }
        }

        private async Task<bool> downloadFromURL(string url, string destination, DownloadProgressChangedEventHandler downloadProgressChanged)
        {
            WebClient webClient = new WebClient();
            webClient.Headers.Add("User-Agent: Mozilla/5.0 (compatible; MSIE 9.0; Windows NT 6.1; WOW64; Trident/5.0)");
            webClient.DownloadProgressChanged += downloadProgressChanged;
            await webClient.DownloadFileTaskAsync(new Uri(url), destination);

            return true;
        }

        private async Task<bool> extractZIP(string path, string destination)
        {
            await Task.Run(() => ZipFile.ExtractToDirectory(path, destination));
            return true;
        }

        private void WebClient_DownloadProgressChanged(object sender, DownloadProgressChangedEventArgs e, string url)
        {
            var grid = (Grid)DependencyGridsList.Children[dependencies.FindIndex(d => d.URL == url) + 1];

            ((ProgressBar)grid.Children[1]).Value = e.ProgressPercentage;
        }

        private void backDepBtn_Click(object sender, RoutedEventArgs e)
        {
            tabControl.SelectedIndex--;
        }

        private void exitBtn_Click(object sender, RoutedEventArgs e)
        {
            if (MessageBox.Show($"Are you sure you want to exit {Config.App.Name}?", "Confirmation", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
            {
                Close();
            }
        }

        private void locChooseFolderBtn_Click(object sender, RoutedEventArgs e)
        {
            var dialog = new VistaFolderBrowserDialog();
            if (dialog.ShowDialog(this).GetValueOrDefault())
            {
                locTxtBox.Text = dialog.SelectedPath;
            }
        }

        private void locNextBtn_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                FileAttributes attr = File.GetAttributes(installDir);

                if (!attr.HasFlag(FileAttributes.Directory)) throw new Exception();
            }
            catch (Exception)
            {
                MessageBox.Show("The path you chose is not a valid folder", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                locNextBtn.IsEnabled = false;
                return;
            }

            tabControl.SelectedItem = DependenciesDownloadTab;
            depCompleted = 0;
            downloadDepBtn.IsEnabled = true;

            loadingDepLbl.Visibility = Visibility.Visible;
            DependencyGridsList.Visibility = Visibility.Collapsed;

            buildDependenciesGridRowDefinitions();

            if (!Config.ReleasesDependencies.rds.ContainsKey(selectedRelease.TagName))
            {
                MessageBox.Show("This release is not supported by this Manager. Check if a new release of the Manager is available.", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                backDepBtn_Click(null, null);
            }

            dependencies = Config.ReleasesDependencies.rds.GetValueOrDefault(selectedRelease.TagName);

            Grid template = templateDepGrid;

            var children = DependencyGridsList.Children;

            buildDependenciesGridRowDefinitions(dependencies.Count + 1);

            for (ushort i = 0; i < dependencies.Count; i++)
            {
                string gridXaml = XamlWriter.Save(template);
                StringReader stringReader = new StringReader(gridXaml);
                XmlReader xmlReader = XmlReader.Create(stringReader);
                Grid newGrid = (Grid)XamlReader.Load(xmlReader);
                newGrid.SetValue(Grid.RowProperty, i + 1);
                ((Label)newGrid.Children[0]).Content = dependencies[i].Name;
                ((Label)newGrid.Children[2]).Content = "";
                children.Add(newGrid);
            }

            template.Visibility = Visibility.Collapsed;

            loadingDepLbl.Visibility = Visibility.Collapsed;
            DependencyGridsList.Visibility = Visibility.Visible;
        }

        private void locTxtBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (locTxtBox.Text.Length > 0)
            {
                installDir = locTxtBox.Text;
                locNextBtn.IsEnabled = true;
            }
            else
            {
                installDir = null;
                locNextBtn.IsEnabled = false;
            }
        }

        private void mslNextBtn_Click(object sender, RoutedEventArgs e)
        {

        }

        private void mslBackBtn_Click(object sender, RoutedEventArgs e)
        {

        }

        private void nextDepBtn_Click(object sender, RoutedEventArgs e)
        {
            tabControl.SelectedIndex++;
            downloadMsl();
        }

        private void downloadMsl()
        {
            string dataDir = Path.Join(installDir, "data");
            string appDir = Path.Join(installDir, "app");
            string tmpDir = Path.Join(installDir, "tmp");
            string fileName = Path.Join(installDir, "app.zip");

            string[] resettable = { appDir, tmpDir, fileName };
            foreach (string entry in resettable)
            {
                if (Directory.Exists(entry))
                {
                    Directory.Delete(entry, true);
                }
                else if (File.Exists(entry))
                {
                    File.Delete(entry);
                }
            }

            downloadFromURL(
                selectedRelease.assets.sources.Find(s => s.format.Equals("zip")).url,
                fileName,
                (sender, e) => mslPB.Value = e.ProgressPercentage
                ).ContinueWith(r =>
           {
               Dispatcher.Invoke(() =>
               {
                   mslStatusLbl.Content = "Download completed. Extracting...";
                   mslPB.IsIndeterminate = true;
               });

               extractZIP(fileName, tmpDir).ContinueWith(z =>
               {
                   Dispatcher.Invoke(() =>
                   {
                       mslStatusLbl.Content = "Extracted. Configuring...";
                   });

                   File.Delete(fileName);

                   var directories = Directory.EnumerateDirectories(tmpDir, "*");
                   var dir = directories.First();

                   Directory.Move(dir, appDir);

                   Directory.Delete(tmpDir);

                   Dispatcher.Invoke(() =>
                   {
                       mslStatusLbl.Content = "Installing project dependencies...";
                   });

                   InstallDeps ids = new(appDir, Path.Join(installDir, "deps", "php"));
                   ids.execute().ContinueWith(rIDs =>
                   {
                       Dispatcher.Invoke(() =>
                       {
                           tabControl.SelectedIndex++;
                       });
                   });
               });
           });
        }

        private void completedBtn_Click(object sender, RoutedEventArgs e)
        {
            Manager.Main manager = new(installDir);
            manager.Show();
            Close();
        }
    }
}
