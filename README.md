![Welcome screen](assets/images/image-20210406173505869.png)

**A quick, easy and lightweight way to install, update and manage your [Media Server Lite](https://msl.francescosorge.com)**



## Introduction

Media Server Lite Manager can automatically install, configure and manage your Media Server Lite on Windows.

- Install the right version of Media Server Lite for you: multiple versions are available
- Automatically installs and configures application's dependencies
- Simple Start/Stop button
- Easy folder sharing
- Lightweight: the Manager is only 1.5MB and the application itself is about 120MB



## Download

You can download Media Server Lite Manager from the [Releases](https://gitlab.com/media-server-lite/media-server-lite-manager/-/releases) page on GitLab, unzip the file and execute *Media Server Lite Standalone.exe*.



## What does it install

It installs a local version of Apache 2.4, PHP 7,4 and the project itself. An administrator account may be required to install Media Server Lite since we also install Apache as a Windows service.



## Manual mode

I also provide official Docker Images for Media Server Lite that you can pull from [Docker Hub](https://hub.docker.com/r/fsorge/msl).



## Screenshots

### Installation process

![Dependencies installation](assets/images/image-20210406174036021.png)

![Application installation](assets/images/image-20210406174102491.png)



### Management

![Main management window](assets/images/image-20210406174251801.png)

![Media management window](assets/images/image-20210406174454661.png)
